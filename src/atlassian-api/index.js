const axios = require("axios").default;
const fs = require("fs");
const backupStore = require('../backupstore');

let MAX_WAIT_MINUTES = 120;
let SLEEP_SECONDS = 10;

async function checkConfluenceBackupState (output, instanceURL, email, token) {
    var res = await axios.get(
        instanceURL+"/wiki/rest/obm/1.0/getprogress.json",
        {
            auth: {
                username: email,
                password: token
            }
        }
    );
    
    if (res.status != 200) {
        throw new Error("Checking backup progress on "+instanceURL+" returned non-200 status: ", res.data);
    }

    output.debug(res.data);

    // Parse the percentage - values like "Progress: 170% longer than last time" can happen!
    var perc = 0;
    var m = res.data.alternativePercentage.match(/(\d+)%/);
    if (m) perc = parseInt(m[1]);
    if (perc > 100) perc = 80;
    if (perc < 0) perc = 0;

    output.debug("Calculated percentage: ", perc);

    output.progress("Backup Creation", perc, res.data.currentStatus);
    return {
        state: perc,
        fileName: res.data.fileName
    };
}

async function checkJiraBackupState (output, instanceURL, email, token, taskId) {
    var res = await axios.get(
        instanceURL+"/rest/backup/1/export/getProgress?taskId="+taskId,
        {
            auth: {
                username: email,
                password: token
            }
        }
    );
    
    if (res.status != 200) {
        throw new Error("Checking backup progress on "+instanceURL+" returned non-200 status: ", res.data);
    }

    output.debug(res.data);

    output.progress("Backup Creation", res.data.progress, res.data.message);
    return {
        state: res.data.progress,
        fileName: res.data.result
    };
}

async function waitForConfluenceBackup (output, instanceURL, email, token) {
    var info = null;
    var startStamp = Date.now();

    while (true != false) {
        // Fetch the percentage of the process
        info = await checkConfluenceBackupState(output, instanceURL, email, token);

        if (info.fileName) 
            return info.fileName;

        // Check if we are waiting more minutes than MAX_WAIT_MINUTES - if yes, throw out
        if (Date.now() - startStamp > MAX_WAIT_MINUTES*60*1000) {
            throw new Error("Waited for backup creation more than "+MAX_WAIT_MINUTES+" minutes - aborting!");
        }

        // Wait for SLEEP_SECONDS before we query again
        await new Promise((resolve) => setTimeout(resolve, SLEEP_SECONDS*1000));
    }
}

async function waitForJiraBackup (output, instanceURL, email, token, taskId) {
    var info = null;
    var startStamp = Date.now();

    while (true != false) {
        // Fetch the percentage of the process
        info = await checkJiraBackupState(output, instanceURL, email, token, taskId);

        if (info.fileName) 
            return info.fileName;

        // Check if we are waiting more minutes than MAX_WAIT_MINUTES - if yes, throw out
        if (Date.now() - startStamp > MAX_WAIT_MINUTES*60*1000) {
            throw new Error("Waited for backup creation more than "+MAX_WAIT_MINUTES+" minutes - aborting!");
        }

        // Wait for SLEEP_SECONDS before we query again
        await new Promise((resolve) => setTimeout(resolve, SLEEP_SECONDS*1000));
    }
}

async function downloadBackup (output, fileURL, dstPath, email, token) {
    try {
        // Let axios fetch a stream response from the download URL
        const res = await axios.get(
            fileURL,
            {
                auth: {
                    username: email,
                    password: token
                },
                responseType: "stream"
            }
        );

        if (res.status != 200)
            throw new Error("Download backup request returned non-200 status: "+res.data);

        // Attach a progress bar to the download
        const totalLength = parseInt(res.headers['content-length']);
        let byteCount = 0;

        res.data.on('data', (chunk) => {
            byteCount += chunk.length;
            // output.debug("Received ", byteCount, " / ", totalLength, " bytes = ", parseInt((byteCount/totalLength)*100), "%");
            output.progress("Backup Download", parseInt((byteCount/totalLength)*100));
        });

        // Create a write stream for the temp file and stream the response in there
        const writer = fs.createWriteStream(dstPath);
        res.data.pipe(writer);

        // Make the caller wait for the promise of a completely written file.
        return new Promise((resolve, reject) => {
            writer.on('finish', () => {
                resolve(dstPath);
            });
            writer.on('error', reject);
        })
    }
    catch (ex) {
        throw ex;
    }
}

async function createConfluenceBackup (output, subdomain, email, token, includeAttachments) {
    var instanceURL = "https://"+subdomain+".atlassian.net";
    output.header("Creating new Confluence backup for "+instanceURL);

    const now = new Date();

    // Start the backup creation process in the Atlassian cloud. Nothing is returned except "yay,200!".
    output.log("... requesting new backup creation.");
    var res = await axios.post(
        instanceURL+'/wiki/rest/obm/1.0/runbackup',
        {
            "cbAttachments": includeAttachments
        },
        {
            headers: {
                "X-Atlassian-Token": "no-check",
                "X-Requested-With": "XMLHttpRequest",
                "Content-Type": "application/json"
            },
            auth: {
                username: email,
                password: token
            }
        }
    );
    
    if (res.status != 200) {
        throw new Error("Starting the backup on "+instanceURL+" returned a non-200 status: ", res.data);
    }

    // Wait for the backup to finish by querying again and again, up to MAX_WAIT_MINUTES mins.
    var path = await waitForConfluenceBackup(output, instanceURL, email, token);
    output.info("... now, the finished backup is available at "+path+"!");
    var downloadPath = instanceURL+"/wiki/download/"+path;

    // Download the finished ZIP file from the cloud to the local file
    output.log("... downloading finished backup to disk. Please stay patient.");

    // Creates the path in the backup store
    var dstPath = backupStore.getBackupDestPath(now, subdomain, "confluence");

    await downloadBackup(output, downloadPath, dstPath, email, token);
    output.info("... backup file is now stored at ", dstPath);
    output.result("... success - backup saved locally.");

    return true;
}

async function createJiraBackup (output, subdomain, email, token, includeAttachments) {
    var instanceURL = "https://"+subdomain+".atlassian.net";
    output.header("Creating new Jira backup for "+instanceURL);

    const now = new Date();

    // Start the backup creation process in the Atlassian cloud. Nothing is returned except "yay,200!".
    output.log("... requesting new backup creation.");
    var res = null;
    try {
        res = await axios.post(
            instanceURL+'/rest/backup/1/export/runbackup',
            {
                "cbAttachments": includeAttachments,
                exportToCloud: true
            },
            {
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                },
                auth: {
                    username: email,
                    password: token
                }
            }
        );
    }
    catch (ex) {
        if (ex && ex.response) {
            if (ex.response.status == 412 && ex.response.data.error)
                throw Error("Jira backup creation could not start because another backup is already in progress. Check if you run this multiple times in parallel? Details: "+ex.response.data.error);
            else
                throw Error("Unknown error when starting a new backup. Status Code "+ex.response.status+". Details: "+ex.response.data);
        }
        throw Error("Unknown error when requesting backup creation. Details: "+ex);
    }

    if (res.status != 200) {
        throw new Error("Starting the backup on "+instanceURL+" returned a non-200 status: ", res.data);
    }

    var taskId = res.data.taskId;
    output.debug("... created task id ", taskId);
    output.log("... started Jira backup creation.");
    
    // Wait for the backup to finish by querying again and again, up to MAX_WAIT_MINUTES mins.
    var path = await waitForJiraBackup(output, instanceURL, email, token, taskId);
    output.info("... now, the finished backup is available at "+path+"!");
    var downloadPath = instanceURL+"/plugins/servlet/"+path;

    // Download the finished ZIP file from the cloud to the local file
    output.log("... downloading finished backup to disk. Please stay patient.");

    // Creates the path in the backup store
    var dstPath = backupStore.getBackupDestPath(now, subdomain, "jira");

    await downloadBackup(output, downloadPath, dstPath, email, token);
    output.info("... backup file is now stored at ", dstPath);
    output.result("... success - backup saved locally.");

    return true;
}

module.exports = {
    createConfluenceBackup: createConfluenceBackup,
    createJiraBackup: createJiraBackup,
    setMaxWaitMinutes: (v) => {
        MAX_WAIT_MINUTES = v;
    },
    setSleepSeconds: (v) => {
        SLEEP_SECONDS = v;
    }
};
