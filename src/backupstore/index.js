const fs = require("fs");
const path = require("path");
const fg = require('fast-glob');
const opsgenieAPI = require('../opsgenie-api');

// Guaranteed to end in slash!
let BACKUPS_BASE_DIR = './backups/';
let RETAIN_COUNT = 7;
let MAX_AGE_HOURS = 192;
let MAX_SIZE_SHRINK_PERCENT = 10;

let lastAssertProblem = "";

function getBackupDestPath (now, label, type) {

    let m = now.getMonth()+1;
    if (m < 10) m = "0"+m;
    let d = now.getDate();
    if (d < 10) d = "0"+d;
    let h = now.getHours();
    if (h < 10) h = "0"+h;
    let i = now.getMinutes();
    if (i < 10) i = "0"+i;


    const ymd = now.getFullYear()+"-"+m+"-"+d+"-"+h+"-"+i;
    var fn = "backup-"+type+"-"+label+"-"+ymd+".zip";
    const destPath = path.resolve(BACKUPS_BASE_DIR, type, label);
    const destFile = path.resolve(destPath, fn);
    fs.mkdirSync(destPath, {recursive: true});

    return destFile;
}

async function manageRetentionForFolder (output, folder, dryrun=false) {
    output.info("... folder: ", folder);

    // Get all backup files in the directory and sort them - newest FIRST
    var files = await fg(folder+"backup-*");
    files.sort(function(a,b) {
        return fs.statSync(b).mtime.getTime() - fs.statSync(a).mtime.getTime();
    });

    // All files behind idx RETAIN_COUNT should die
    var deprecatedFiles = files.slice(RETAIN_COUNT);
    if (!deprecatedFiles.length)
        return 0;

    for (var i = 0; i < deprecatedFiles.length; i++) {
        if (!dryrun) {
            output.info("...... unlinking ", deprecatedFiles[i]);
            fs.unlinkSync(deprecatedFiles[i]);
        }
        else
            output.log("...... would unlink ", deprecatedFiles[i], " if this was no dry run.");
    }

    return deprecatedFiles.length;
}

async function rotate (output, dryrun=false) {
    var pattern = BACKUPS_BASE_DIR+"*/*";
    output.debug("Scanning for directories with pattern", pattern);
    const entries = await fg(pattern, {onlyDirectories: true, markDirectories: true});

    var killCount = 0;
    output.header("Managing backup retention for "+entries.length+" sites.");
    for (var i = 0; i < entries.length; i++) {
        killCount += await manageRetentionForFolder(output, entries[i], dryrun);
    }
    output.result("... Done, removed "+killCount+" deprecated backups.");
}

async function assertBackupAgeAndSize (output, label, type) {
    output.info("... asserting site", type+"/"+label);
    lastAssertProblem = "";

    // Find all backup files and sort them by modification date, DESC
    var files = await fg(BACKUPS_BASE_DIR+type+"/"+label+"/backup-*");
    if (!files.length) {
        lastAssertProblem = "Problem! No backups for site "+type+"/"+label+" found!";
        output.err(lastAssertProblem);
        return false;
    }

    files.sort(function(a,b) {
        return fs.statSync(b).mtime.getTime() - fs.statSync(a).mtime.getTime();
    });

    // We have a youngest backup!
    var youngestFile = files[0];
    output.debug("... youngest file for site", type+"/"+label, "is", youngestFile);

    // Check the age of the youngest backup - it may not be older than MAX_AGE_HOURS
    var youngestStamp = fs.statSync(youngestFile).mtime.getTime();
    var now = Date.now();

    if (now - youngestStamp > MAX_AGE_HOURS*60*60*1000) {
        lastAssertProblem = "Problem! Freshest backup '"+youngestFile+"' for site "+type+"/"+label+" is older than "+MAX_AGE_HOURS+" hours!";
        output.err(lastAssertProblem);
        return false;
    }

    output.info("... age is fine: backup file", youngestFile, "is newer than", MAX_AGE_HOURS, "hours");

    // Optional check for size change: newest backup may not be more than MAX_SIZE_SHRINK_PERCENT% smaller without warning.
    if (files.length > 1) {
        var secondFile = files[1];

        var ysize = fs.statSync(youngestFile).size;
        var ssize = fs.statSync(secondFile).size;
        var limit = parseInt(ssize * (1.0 - MAX_SIZE_SHRINK_PERCENT/100));

        output.debug("Comparing new size ", ysize, "to old size", ssize," - limit is ", limit);

        if (ysize < limit) {
            lastAssertProblem = "Problem! Freshest backup '"+youngestFile+"' for site "+type+"/"+label+" is more than "+MAX_SIZE_SHRINK_PERCENT+"% smaller than previous backup '"+secondFile+"'!";
            output.err(lastAssertProblem);
            return false;
        }

        output.info("... size is fine: backup file", youngestFile, "is bigger than", limit, "bytes");
    }
    else
        output.info("... skipping size check as", youngestFile, "is the first and only backup.");

    return true;
}

async function assertSites (output, sites, createIncidents=false) {
    let problemCount = 0;
    output.header("Asserting status of", sites.length, "sites.");

    for (var i = 0; i < sites.length; i++) {
        const res = await assertBackupAgeAndSize(output, sites[i].label, sites[i].type);
        if (!res) {
            if (createIncidents) {
                var siteLabel = sites[i].type+"/"+sites[i].label;
                await opsgenieAPI.createAlert(output, siteLabel, "Atlassian Cloud Backup Problem with "+siteLabel, lastAssertProblem, {site: siteLabel});
            }
            problemCount++;
        } 
    }

    output.result("Checked", sites.length, "sites:", problemCount, "problems found.");
    return problemCount;
}

module.exports = {
    getBackupDestPath: getBackupDestPath,
    rotate: rotate,
    assertBackupAgeAndSize: assertBackupAgeAndSize,
    assertSites: assertSites,

    setBaseDir: (v) => {
        BACKUPS_BASE_DIR = v;
    },
    setRetainCount: (v) => {
        RETAIN_COUNT = v;
    },
    setMaxAgeHours: (v) => {
        MAX_AGE_HOURS = v;
    },
    setMaxSizeShrinkPercent: (v) => {
        MAX_SIZE_SHRINK_PERCENT = v;
    },
    getLastAssertProblem: () => {
        return lastAssertProblem;
    }
};