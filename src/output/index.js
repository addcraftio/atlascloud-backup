const chalk = require("chalk");
const cliProgress = require('cli-progress');

var VERBOSE_LEVEL = 0;

var progressRegistry = {};

module.exports = {
    logTitle: () => {
        console.log(
            chalk.yellow(
              'atlascloud-backup'
            )
        );
    },

    progress: function(label, perc, info) {
        if (!progressRegistry[label]) {
            progressRegistry[label] = new cliProgress.SingleBar({
                format: "... "+label+' [{bar}] {percentage}% | ETA: {eta}s | {info}'
            });
            progressRegistry[label].start(100, 0, {
                info: ""
            });
        }

        progressRegistry[label].update(perc, {
            info: info
        });

        if (perc >= 100) {
            progressRegistry[label].stop();
            delete progressRegistry[label];
        }
    },

    setVerbose: (vlevel) => {
        VERBOSE_LEVEL = vlevel;
    },

    err: function() {
        console.log(chalk.bold.red.apply(chalk.bold.red, arguments));
    },

    log: function() {
        console.log.apply(console, arguments);
    },
    warn: function() {
        VERBOSE_LEVEL >= 0 && console.log(chalk.bold.yellow.apply(chalk.bold.yellow, arguments));
    },
    info: function() {
        VERBOSE_LEVEL >= 1 && console.log.apply(console, arguments);
    },
    debug: function() {
        VERBOSE_LEVEL >= 2 && console.log(chalk.gray.apply(chalk.bold.gray, arguments));;
    },

    header: function() {
        console.log(chalk.bold.bgGray.apply(chalk.bold.bgGray, arguments));
    },
    result: function() {
        console.log(chalk.bold.bgGray.apply(chalk.bold.bgGray, arguments));
    }
};